<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板システム</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>

   <body>

<a href="signup">ユーザー新規登録</a>
<a href="./">ホーム</a>

<div class="users">

<table border="1">
 <tr>

    <th>ログインID</th>
    <th>氏名</th>
    <th>支店名</th>
    <th>役職名</th>
    <th>ユーザー編集</th>
    <th>アカウント停止</th>
    <th>復活</th>


  </tr>
<c:forEach var="user" items="${users}">

  <tr>

    <td>${user.login_id }</td>
    <td>${user.name }</td>
    <td>

	<c:forEach items="${branches}" var="branch">
	    <c:if test="${user.branch_id == branch.id}">
	    	<div class="name">
	    		<span class="name"><c:out value="${branch.name}" /></span>
	    	</div>
	    </c:if>
    </c:forEach>
    </td>

    <td>

    <c:forEach items="${positions}" var="position">
	    <c:if test="${user.position_id == position.id}">
	    	<div class="name">
	    		<span class="name"><c:out value="${position.name}" /></span>
	    	</div>
	    </c:if>
    </c:forEach>

    </td>
    <td><a href="settings?id=${user.id }">設定</a></td>
    <td>
    <c:if test="${user.is_deleted == 0 && user.id != editerId}">
	    <form action="usermanagement" method="post">
	    	<input type="hidden" name="id" value="${user.id}">
	    	<input type="submit" value="停止" onClick="return confirm('本当に停止しますか？')">
	    </form>
	</c:if>
    </td>
    <td>
    <c:if test="${user.is_deleted==1}">
	    <form action="accountrevival" method="post">
	    	<input type="hidden" name="id" value="${user.id}">
	    	<input type="submit" value="復活" onClick="return confirm('本当に復活しますか？')">
	    </form>
	</c:if>
    </td>
  </tr>

  </c:forEach>

</table>


            <div class="copylight"> Copyright(c)松田貴大</div>
        </div>
    </body>
</html>