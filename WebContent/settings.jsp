<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${loginUser.login_id}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post"><br />
                <input name="id" value="${editUser.id}" id="id" type="hidden"/>
                <label for="login_id">ログインID</label>
                <input name="login_id" value="${editUser.login_id}" id="login_id"/><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="name">氏名</label>
                <input name="name" value="${editUser.name}" id="name"/>（名前はあなたの公開プロフィールに表示されます）<br />



				<label for="branch_id">支店名</label>
					<select name="branch_id">
						<c:forEach items="${branches}" var="branch">
							<option value="${branch.id}" id="branch_id">${branch.name}</option>
						</c:forEach>
					</select>
				<br/>


				<label for="position_id">役職名</label>
					<select name="position_id">
						<c:forEach items="${positions}" var="position">
							<option value="${position.id}" id="position_id">${position.name}</option>
						</c:forEach>
					</select>
				<br/>

                <input type="submit" value="登録" /> <br />
                <a href="usermanagement">戻る</a>
            </form>
            <div class="copylight"> Copyright(c)松田貴大</div>
        </div>
    </body>
</html>