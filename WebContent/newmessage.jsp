<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>

	<body>

		<c:if test="${ not empty loginUser }">
			<a href="./">ホーム</a>
			<a href="logout">ログアウト</a>
			<a href="usermanagement">ユーザー管理</a>
		</c:if>

	   	<c:if test="${ not empty loginUser }">
			<div class="profile">
			<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
			</div>
		</c:if>
		<c:if test="${ not empty errorMessages }">
        	<div class="errorMessages">
            	<ul>
               		<c:forEach items="${errorMessages}" var="message">
                 		<li><c:out value="${message}" />
                 	</c:forEach>
                </ul>
            </div>
            <c:remove var="errorMessages" scope="session" />
        </c:if>
		<div class="form-area">
			<c:if test="${ not empty loginUser }">
				<form action="newmessage" method="post">

					<label for="title">タイトル</label>
					<input name="title" id="title"><br/>
					<label for="category">カテゴリ</label>
					<input name="category" id="category"><br/>
					本文<br />
					<textarea name="message" cols="100" rows="5" class="tweet-box"></textarea>
					<input type="hidden" name="id" value="${message.id}">
					<br />
					<input type="submit" value="投稿">（1000文字まで）
				</form>
			</c:if>
		</div>

		<div class="copylight"> Copyright(c)松田貴大</div>
	</body>
</html>