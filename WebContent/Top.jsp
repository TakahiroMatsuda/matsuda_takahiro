<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板システム</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>

	<body>
	   <div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">ユーザー新規登録</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="usermanagement">ユーザー管理</a>
				<a href="newmessage">新規投稿</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<form action="./" method="get">

			<label for="category">カテゴリ検索</label>
            <input name="category" value="${category}" id="category">

			<br />
			日時検索
			<br />
			<input type="date" name="start_date" value="${start_date}" id="start_date">～<input type="date" name="end_date" value ="${end_date}" id="end_date">
			<br />
			<input type="submit" value="検索">
		</form>

		<c:if test="${ not empty loginUser }">
			<div class="profile">
			<div class="name"><h1><c:out value="${loginUser.name}" /></h1></div>

			</div>
		</c:if>

		<c:if test="${ not empty errorMessages }">
        	<div class="errorMessages">
            	<ul>
                	<c:forEach items="${errorMessages}" var="message">
                    	<li><c:out value="${message}" />
                    </c:forEach>
                </ul>
            </div>
            <c:remove var="errorMessages" scope="session" />
        </c:if>

		<div class="form-area">
			<c:if test="${ not empty loginUser }">

				<div class="messages">
					<c:forEach items="${messages}" var="message">
						<div class="message">
						<div class="title">
							<span class="title"><h2><c:out value="${message.title}" /></h2></span>
						</div>
						<div class="category-name">
							<span class="name"><c:out value="投稿者：${message.name}" /></span>
							<span class="category"><c:out value="カテゴリー：${message.category}" /></span>
						</div>
						<div class="text"><c:out value="${message.text}" /></div>
						<div class="date"><fmt:formatDate value="${message.created_at}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
						</div>

						<form action="deletemessage" method="post">
							<input type="hidden" name="id" value="${message.id}">
							<input type="submit" value="投稿削除" onClick="return confirm('本当に削除しますか？')">
						</form>
						<br/>
						<form action="comment" method="post">
							コメント<br />
							<textarea name="text" cols="80" rows="1" class="tweet-box"></textarea>
							<input type="hidden" name="message_id" value="${message.id}">
							<br />
							<input type="submit" value="投稿">（500文字まで）
						</form>

						<div class="comments">
							<c:forEach items="${comments}" var="comment">
								<c:if test="${message.id == comment.message_id}">
									<div class="comment">
										<div class="name">
											<span class="name"><c:out value="${comment.name}" /></span>
										</div>
											<div class="text"><c:out value="${comment.text}" /></div>
											<div class="date"><fmt:formatDate value="${comment.created_at}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
									</div>
									<c:if test="${comment.user_id == loginUserId}">
										<form action="deletecomment" method="post">
											<input type="hidden" name="id" value="${comment.id}">
											<input type="submit" value="コメント削除" onClick="return confirm('本当に削除しますか？')">
										</form>
									</c:if>
								</c:if>
							</c:forEach>
						</div>
						<hr>
					</c:forEach>
				</div>

			</c:if>
		</div>



		<div class="copylight"> Copyright(c)松田貴大</div>
		</div>
	</body>
</html>