package task.service;

import static task.utils.CloseableUtil.*;
import static task.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import task.beans.Message;
import task.beans.UserMessage;
import task.dao.MessageDao;
import task.dao.UserMessageDao;


public class MessageService {

    public void register(Message message) {

        Connection connection = null;

        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
           rollback(connection);
            throw e;
        } finally {
            close(connection);
       }
    }

    private static final int LIMIT_NUM = 1000;
    public List<UserMessage> getMessage(String category, String start_date, String end_date) {

        Connection connection = null;

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//        String CurrentTime = String.valueOf(sdf);
        System.out.println(sdf.format(date));
        String CurrentTime = sdf.format(date);
        try {
            connection = getConnection();

            if(StringUtils.isEmpty(start_date)){
            	start_date = "2018-01-01 00:00:00";
            }
            else{
            	start_date = start_date + " 00:00:00";
            }
            if(StringUtils.isEmpty(end_date)){
            	end_date = CurrentTime;
            }
            else{
            	end_date = end_date + " 23:59:59";
            }

            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM, category, start_date, end_date);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void registerDelete(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.delete(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
           rollback(connection);
            throw e;
        } finally {
            close(connection);
       }
    }

}