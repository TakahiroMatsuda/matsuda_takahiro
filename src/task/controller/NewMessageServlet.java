package task.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import task.beans.Message;
import task.beans.User;
import task.service.MessageService;
import task.service.UserService;

@WebServlet(urlPatterns = { "/newmessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<User> users = new UserService().getUsers();

        request.setAttribute("users", users);
//        request.setAttribute("isShowMessageForm", isShowMessageForm);

        request.getRequestDispatcher("newmessage.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setText(request.getParameter("message"));
            message.setTitle(request.getParameter("title"));
            message.setCategory(request.getParameter("category"));
            message.setUser_id(user.getId());


            new MessageService().register(message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("newmessage");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String message = request.getParameter("message");
        String title = request.getParameter("title");
        String category = request.getParameter("category");

        if (30 < title.length()) {
            messages.add("タイトルは30文字以下で入力してください");
        }
        if (10 < category.length()) {
            messages.add("カテゴリは10文字以下で入力してください");
        }
        if (StringUtils.isEmpty(message) == true) {
            messages.add("本文を入力してください");
        }
        if (1000 < message.length()) {
            messages.add("本文は1000文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}