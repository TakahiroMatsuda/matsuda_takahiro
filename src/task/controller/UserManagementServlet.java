package task.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import task.beans.Branch;
import task.beans.Position;
//import task.beans.UserMessage;
//import task.service.MessageService;
import task.beans.User;
import task.service.UserService;

@WebServlet(urlPatterns = { "/usermanagement" })
public class UserManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<User> users = new UserService().getUsers();
    	List<Branch> branches = new UserService().getBranch();
    	List<Position>positions = new UserService().getPosition();
    	HttpSession session = request.getSession();
    	User loginUser = (User) session.getAttribute("loginUser");
    	User editUser = new UserService().getUser(loginUser.getId());
    	int editerId = loginUser.getId();


        request.setAttribute("users", users);
        request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);
        request.setAttribute("editerId", editerId);
        System.out.println("editUser");
        System.out.println(editerId + "editUser");
//        request.setAttribute("isShowMessageForm", isShowMessageForm);

        request.getRequestDispatcher("usermanagement.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> users = new ArrayList<String>();

//        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

//            User user = new User();
            user.setId(Integer.parseInt(request.getParameter("id")));
//            message.setUser_id(user.getId());

            new UserService().stopUpdate(user);

            response.sendRedirect("./");
//        } else {
//            session.setAttribute("errorMessages", messages);
//            response.sendRedirect("./");
//        }
    }
    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("loginUser");
        String position_id = request.getParameter("position_id");

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}