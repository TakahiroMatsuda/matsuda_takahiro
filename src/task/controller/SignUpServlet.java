package task.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import task.beans.Branch;
import task.beans.Position;
import task.beans.User;
import task.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<Branch> branches = new UserService().getBranch();
        request.setAttribute("branches", branches);
        List<Position> positions = new UserService().getPosition();
        request.setAttribute("positions", positions);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> users = new ArrayList<String>();



        HttpSession session = request.getSession();
        if (isValid(request, users) == true) {

        	String login_id = request.getParameter("login_id");
//        	System.out.println(login_id + "重複確認");
//        	User logincheck = new UserService().loginCheck(login_id);
//        	System.out.println(logincheck + "重複確認");


            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setBranch_id(request.getParameter("branch_id"));
            user.setPosition_id(request.getParameter("position_id"));

            new UserService().register(user);

//            check = user.getLogin_id();


            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", users);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String checkPassword = request.getParameter("checkPassword");
        User loginUser = new UserService().loginCheck(login_id);

//        System.out.println(loginUser.getId());
//        if (check.equals(login_id)) {
//            messages.add("そのログインIDは使用済みです");
//        }

        if(loginUser != null){
        	messages.add("そのログインIDはすでに使われています");
        }
        
        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (!login_id.matches("[a-zA-Z0-9]{6,20}")) {
            messages.add("ログインIDは6文字以上20文字以内の半角英数字にしてください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (!password.equals(checkPassword)) {

            messages.add("確認用パスワードが異なっています");
        }
        if (!password.matches("[a-zA-Z0-9]{6,20}")) {
            messages.add("パスワードは6文字以上20文字以内の半角英数字記号にしてください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}