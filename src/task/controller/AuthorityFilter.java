
package task.controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import task.beans.User;

@WebFilter(urlPatterns = {"/usermanagement", "/settings", "/signup"})
public class AuthorityFilter implements Filter{

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

        HttpServletRequest httpRequest = (HttpServletRequest) req;
        HttpServletResponse httpResponse = (HttpServletResponse) res;

//        HttpSession session = httpRequest.getSession();
//        System.out.println(session);

        User user = (User) httpRequest.getSession().getAttribute("loginUser");
        HttpSession session = httpRequest.getSession();

        if(!user.getPosition_id().equals("1")){
        	session.setAttribute("errorMessages", "管理者権限がありません");
        	httpResponse.sendRedirect("./");
        	return;
	    }else{
	    	chain.doFilter(req, res);
	    	System.out.println("AuthorityFilter# chain.doFilterが実行されました");
	    	System.out.println(user.getPosition_id());
	    }
    }

    public void init(FilterConfig config) throws ServletException{
    }
    public void destroy(){
    }
}
