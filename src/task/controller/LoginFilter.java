
package task.controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import task.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter{

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

        HttpServletRequest httpRequest = (HttpServletRequest) req;
        HttpServletResponse httpResponse = (HttpServletResponse) res;

//        HttpSession session = httpRequest.getSession();
//        System.out.println(session);

        User user = (User) httpRequest.getSession().getAttribute("loginUser");

        if(user == null && !httpRequest.getServletPath().equals("/login")){
        	httpResponse.sendRedirect("login");
        	return;
	    }else{
	    	chain.doFilter(req, res);
	    	System.out.println("LoginFilter# chain.doFilterが実行されました");
	    }
    }

    public void init(FilterConfig config) throws ServletException{
    }
    public void destroy(){
    }
}
