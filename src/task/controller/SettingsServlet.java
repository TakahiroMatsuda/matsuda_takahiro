package task.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import task.beans.Branch;
import task.beans.Position;
import task.beans.User;
import task.exception.NoRowsUpdatedRuntimeException;
import task.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        List<Branch> branches = new UserService().getBranch();
        request.setAttribute("branches", branches);
        List<Position> positions = new UserService().getPosition();
        request.setAttribute("positions", positions);

        String userID = request.getParameter("id");
        User editUser = new UserService().getUser(Integer.parseInt(userID));
        request.setAttribute("editUser", editUser);

        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();


        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {

            try {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("settings.jsp").forward(request, response);

            }

            response.sendRedirect("usermanagement");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setName(request.getParameter("name"));
        editUser.setBranch_id(request.getParameter("branch_id"));
        editUser.setPosition_id(request.getParameter("position_id"));
        System.out.println(request.getParameterValues("branch_id") + "支店コード");
        System.out.println(request.getParameter("name") + "ユーザー名");



//        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//        try {
//            editUser.setUpdatedDate(sdFormat.parse(request.getParameter("updatedDate")));
//        } catch (Exception e) {
//        	e.printStackTrace();
//            return null;
//        }
        return editUser;
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        User loginUser = new UserService().loginCheck(login_id);
        System.out.println(password + "チェック");
        String editorLogin_id = request.getParameter("login_id");

        if(loginUser != null && loginUser.equals(editorLogin_id)){
        	messages.add("そのログインIDはすでに使われています");
        }
        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("アカウント名を入力してください");
        }
        if (!login_id.matches("[a-zA-Z0-9]{6,20}")) {
            messages.add("ログインIDは6文字以上20文字以内の半角英数字にしてください");
        }
        if(StringUtils.isEmpty(password) == false){
        	if (!password.matches("[a-zA-Z0-9]{6,20}")) {
                messages.add("パスワードは6文字以上20文字以内の半角英数字記号にしてください");
            }
        }
        System.out.println(name);
        if (!name.matches("^[^-~｡-ﾟ]{1,10}$")) {
            messages.add("ユーザー名称は10文字以下にしてください");
        }

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}