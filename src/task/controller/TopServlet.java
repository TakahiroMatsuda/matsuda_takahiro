package task.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import task.beans.User;
import task.beans.UserComment;
import task.beans.UserMessage;
import task.service.CommentService;
import task.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

//        Message message = new Message();
//        String userID = request.getParameter("id");
        String category = request.getParameter("category");
        String start_date = request.getParameter("start_date");
        String end_date = request.getParameter("end_date");

        List<UserMessage> messages = new MessageService().getMessage(category, start_date, end_date);

        request.setAttribute("messages", messages);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.setAttribute("category", category);
        request.setAttribute("start_date", start_date);
        request.setAttribute("end_date", end_date);

        List<UserComment> comments = new CommentService().getComment();

        request.setAttribute("comments", comments);

        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");
        int loginUserId = loginUser.getId();
        request.setAttribute("loginUserId", loginUserId);

        request.getRequestDispatcher("/Top.jsp").forward(request, response);
    }
}