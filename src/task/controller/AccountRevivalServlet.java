package task.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import task.beans.UserMessage;
//import task.service.MessageService;
import task.beans.User;
import task.service.UserService;

@WebServlet(urlPatterns = { "/accountrevival" })
public class AccountRevivalServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> users = new ArrayList<String>();

//        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

//            User user = new User();
            user.setId(Integer.parseInt(request.getParameter("id")));
//            message.setUser_id(user.getId());

            new UserService().restartUpdate(user);

            response.sendRedirect("./");
//        } else {
//            session.setAttribute("errorMessages", messages);
//            response.sendRedirect("./");
//        }
    }

}