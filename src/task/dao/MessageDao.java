package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import task.beans.Message;
import task.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("id");
            sql.append(", title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(",user_id");
            sql.append(") VALUES (");
            sql.append(" ?"); // id
            sql.append(", ?"); // title
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", CURRENT_TIMESTAMP"); // created_at
            sql.append(", CURRENT_TIMESTAMP"); // updated_at
            sql.append(", ?"); // user_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            System.out.println(ps);

            ps.setInt(1, message.getId());
            ps.setString(2, message.getTitle());
            ps.setString(3, message.getText());
            ps.setString(4, message.getCategory());
            ps.setInt(5, message.getUser_id());

            System.out.println(ps);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM messages WHERE");
            sql.append(" id = ?");
//            sql.append(") VALUES (");
//            sql.append(" ?"); // id


            ps = connection.prepareStatement(sql.toString());

            System.out.println(ps);

            ps.setInt(1, message.getId());
//            ps.setString(2, message.getTitle());
//            ps.setString(3, message.getText());
//            ps.setString(4, message.getCategory());
//            ps.setInt(5, message.getUser_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}