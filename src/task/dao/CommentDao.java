package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import task.beans.Comment;
import task.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("id");
            sql.append(", text");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(", user_id");
            sql.append(", message_id");
            sql.append(") VALUES (");
            sql.append(" ?"); // id
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_at
            sql.append(", CURRENT_TIMESTAMP"); // updated_at
            sql.append(", ?"); // user_id
            sql.append(", ?"); // message_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getId());
            ps.setString(2, comment.getText());
            ps.setInt(3, comment.getUser_id());
            ps.setInt(4, comment.getMessage_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments WHERE");
            sql.append(" id = ?");
//            sql.append(") VALUES (");
//            sql.append(" ?"); // id


            ps = connection.prepareStatement(sql.toString());

            System.out.println(ps);

            ps.setInt(1, comment.getId());
//            ps.setString(2, message.getTitle());
//            ps.setString(3, message.getText());
//            ps.setString(4, message.getCategory());
//            ps.setInt(5, message.getUser_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}