package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import task.beans.User;
import task.exception.NoRowsUpdatedRuntimeException;
import task.exception.SQLRuntimeException;

public class UserDao {

	public User getUser(Connection connection, String login_id,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_deleted = 0";

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);
            ps.setString(2, password);

            System.out.println(ps.toString());

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public User getLoginId(Connection connection, String login_id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);

            System.out.println(ps.toString() + "重複確認");

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public List<User> getUsers(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users";

            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else {
                return userList;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                String branch_id = rs.getString("branch_id");
                String position_id = rs.getString("position_id");
                String password = rs.getString("password");
                int is_deleted = rs.getInt("is_deleted");
                Timestamp created_at = rs.getTimestamp("created_at");
                Timestamp updated_at = rs.getTimestamp("updated_at");


                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setPassword(password);
                user.setIs_deleted(is_deleted);
                user.setCreated_at(created_at);
                user.setUpdated_at(updated_at);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(", is_deleted");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", CURRENT_TIMESTAMP"); // created_at
            sql.append(", CURRENT_TIMESTAMP"); // updated_at
            sql.append(", ?"); // is_deleted
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            System.out.println(ps.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setString(4, user.getBranch_id());
            ps.setString(5, user.getPosition_id());
            ps.setInt(6, user.getIs_deleted());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            if (user.getPassword() != null) {
            	sql.append(", password = ?");
            }
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(", created_at = CURRENT_TIMESTAMP");
            sql.append(", updated_at = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());

            if (user.getPassword() == null) {
            	ps.setString(2, user.getName());
                ps.setString(3, user.getBranch_id());
                ps.setString(4, user.getPosition_id());
                ps.setInt(5, user.getId());
            } else {
            	ps.setString(2, user.getPassword());
                ps.setString(3, user.getName());
                ps.setString(4, user.getBranch_id());
                ps.setString(5, user.getPosition_id());
                ps.setInt(6, user.getId());
            }


//            ps.setTimestamp(7, new Timestamp(user.getUpdatedDate().getTime()));

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

    public User getUser(Connection connection, int id) {
    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		} else if (2 <= userList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		} else {
    			return userList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    			close(ps);
    	}
    }

    public void stopUpdate(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  is_deleted = 1");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, user.getId());

//            ps.setTimestamp(7, new Timestamp(user.getUpdatedDate().getTime()));

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

    public void restartUpdate(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  is_deleted = 0");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, user.getId());

            System.out.println(ps.toString() + "確認");

//            ps.setTimestamp(7, new Timestamp(user.getUpdatedDate().getTime()));

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

}