package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import task.beans.UserComment;
import task.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.created_at as created_at, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("users.name as name, ");
            sql.append("users.id ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON user_id = users.id ");
            sql.append("ORDER BY created_at DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String text = rs.getString("text");
                Timestamp created_at = rs.getTimestamp("created_at");
                int message_id = rs.getInt("message_id");


                UserComment comment = new UserComment();
                comment.setName(name);
                comment.setId(id);
                comment.setUser_id(user_id);
                comment.setText(text);
                comment.setCreated_at(created_at);
                comment.setMessage_id(message_id);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}