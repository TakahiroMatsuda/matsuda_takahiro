package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import task.beans.Position;
import task.exception.SQLRuntimeException;

public class PositionDao {

    public List<Position> getPosition(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM positions");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Position> ret = toUserPositionList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Position> toUserPositionList(ResultSet rs)
            throws SQLException {

        List<Position> ret = new ArrayList<Position>();
        try {
            while (rs.next()) {
            	String name = rs.getString("name");
                int id = rs.getInt("id");

                Position position = new Position();
                position.setName(name);
                position.setId(id);


                ret.add(position);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}