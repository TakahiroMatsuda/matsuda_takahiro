package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import task.beans.UserMessage;
import task.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num, String category, String start_date, String end_date) {
    	System.out.println(start_date);
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.created_at as created_at, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("users.id ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON user_id = users.id ");
            sql.append("WHERE messages.created_at BETWEEN ? AND ? ");
            if(!StringUtils.isEmpty(category)){
            	sql.append("AND messages.category LIKE ? ");
            }
            sql.append("ORDER BY created_at DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());
            System.out.println(ps.toString());
            System.out.println(!StringUtils.isEmpty(category));
            System.out.println(category);

	        ps.setString(1, start_date);
	        ps.setString(2, end_date);
	        if(!StringUtils.isEmpty(category )){
	        	ps.setString(3, "%" + category + "%");
	        }

	        System.out.println(ps.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String title = rs.getString("title");
                String name = rs.getString("name");
                String category = rs.getString("category");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String text = rs.getString("text");
                Timestamp created_at = rs.getTimestamp("created_at");


                UserMessage message = new UserMessage();
                message.setTitle(title);
                message.setName(name);
                message.setCategory(category);
                message.setId(id);
                message.setUser_id(user_id);
                message.setText(text);
                message.setCreated_at(created_at);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}